const http = require('http');
const express = require('express');
const app = express();

http.createServer(function(req, res) {
    res.writeHead(200, {
        'Content-Type': 'text/plain'
    });
    res.end('Hello World!');
}).listen(3000);

console.log('Server running at localhost:3000');